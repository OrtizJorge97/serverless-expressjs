const express = require('express')
const url2pdf = require('url2pdf');
const Handlebars = require('handlebars');
const moment = require('moment-timezone');
const path = require('path');
const fs = require('fs');

const {
    createPdfFromHtmlString
} = require('./utils/documents')

const app = express()
const port = 3007

const paymentParams = {
    "paymentId": "jl5m6zp2cfm3jbd2hciyamws",
    "payerName": "Jorge Ortiz C97 gmail",
    "payerEmail": "jorge.ortiz.c97@gmail.com",
    "paymentDate": "06/01/2023 3:31:04 AM",
    "merchantCompanyName": "Receivables 2.0_USA2",
    "merchantContactName": "test customer",
    "merchantContactEmail": "jfortiz@paystand.com",
    "merchantBillingAddress": "1 paystand St, #2, Santa Cruz, CA, USA",
    "payerBillingAddress": "Fdfdf, Fdfd, BDS, AFG",
    "payerTotalFees": 2.16,
    "customRate": 1,
    "customFlat": 2,
    "discount": null,
    "totalAmount": 18.16,
    "paymentMethod": "Fdfdf (*4242) | visa",
    "sageInvoices": [
        {
            "paymentId": "jl5m6zp2cfm3jbd2hciyamws",
            "invoiceId": "IN11797",
            "receivableId": "dqcs5q3j6pjqn7bxxh6ta5jd",
            "receivableAmount": 32,
            "receivableAmountPaid": 16,
            "receivableDateDue": "05/11/2023 7:00:00 AM",
            "transAmountApplied": 16,
            "paymentDatePosted": "2023-06-01T10:31:13.000Z",
            "statusPaid": "Partially Paid"
        }
    ],
    "subTotal": 16
}

app.get('/payment/:paymentId/pdf', async (req, res) => {
    const { paymentId } = req.params;
    console.log('TEST FOR DEPLOY')
    console.log('deploy')
    console.log('paymentParams', paymentParams);
    console.log('paymentParams.paymentDate string', moment(moment(paymentParams.paymentDate).toLocaleString()).format('MM/DD/YYYY h:mm:ss A'));// format("MM/DD/YYYY h:mm:ss A"));

    const paymentReceiptTemplatePath = path.join(__dirname, './templates/payment-receipt.handlebars');
    console.log('paymentReceiptTemplatePath', paymentReceiptTemplatePath);
    const paymentTemplate = await fs.promises.readFile(paymentReceiptTemplatePath, 'utf8');

    // paymentTemplate = data.toString('utf-8')
    // console.log('paymentTemplate', paymentTemplate.toString())
    // var paymentTemplate = handlebars.compile(paymentTemplate.toString())
    const htmlString = Handlebars.compile(paymentTemplate)(paymentParams);
    console.log('htmlString', htmlString);

    const pdf = await createPdfFromHtmlString(htmlString);

    const bufferToBase64 = pdf.toString('base64');
    /*
    const pdfPath = await url2pdf.renderFromHTML(htmlString, {
        paperSize: {
            format: 'Letter',
            orientation: 'portrait',
            margin: {
                top: '2cm',
                bottom: '2cm',
            },
        },
        saveDir: '/tmp',
        idLength: 10,
        loadTimeout: 800,
        autoCleanFileAgeInSec: -1,
    });

    const pdfBuffer = fs.readFileSync(pdfPath) || {};
    const bufferToBase64 = Buffer.from(pdfBuffer).toString('base64');*/
    res.status(200).send({
        content: bufferToBase64,
        type: 'application/pdf',
        name: `payment-${paymentId}-${Date.now()}-receipt.pdf`
    });
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})

module.exports = app;