const sls = require('serverless-http');
const app = require('./application');

exports.handler = sls(app);
