const chromium = require('chrome-aws-lambda');
const agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36';

module.exports = {
    createPdfFromHtmlString: async (htmlString) => {
        /*
        const context = await puppeteer.launch({
            executablePath: await chromium.executablePath,
            headless: chromium.headless,
            ignoreHTTPSErrors: true,
            defaultViewport: chromium.defaultViewport,
            ignoreDefaultArgs: ['--disable-extensions'],
            args: [
                '--disable-gpu',
                '--disable-dev-shm-usage',
                '--disable-setuid-sandbox',
                '--no-sandbox',
                '--single-process',
                '--hide-scrollbars',
                '--disable-web-security',
                '--mute-audio',
                '--disable-web-security',
            ],
            devtools: false,
        });*/
        //const chrome = await getChrome();

        const context = await chromium.puppeteer.launch({
            args: chromium.args,
            defaultViewport: chromium.defaultViewport,
            executablePath: await chromium.executablePath,
            headless: chromium.headless,
            ignoreHTTPSErrors: true,
        });

        console.log('[request-create-page]');
        const page = await context.newPage();
        await page.setUserAgent(agent)

        console.log('[request-set-content]');
        await page.setContent(htmlString, {
            waitUntil: ['load', 'domcontentloaded', 'networkidle0'],
        });

        console.log('[request-emulate-media-type]');
        await page.emulateMediaType('screen');

        console.log('[request-create-pdf-file]');
        const pdf = await page.pdf({ // return the pdf buffer.
            margin: {
                top: '2cm', bottom: '2cm', left: '1cm', right: '1cm',
            },
            printBackground: true,
            format: 'letter',
            scale: 1.3,
        });
        await context.close();

        return pdf;
    }
}